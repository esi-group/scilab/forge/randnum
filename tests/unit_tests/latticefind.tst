// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Find the couples (c1,c2) and
// plots the lines for a=3,m=31
a = 3;
m = 31;
[linesnb,couples] = randnum_latticefind(a,m,2,20);
couples_e  = [ 
  -3.    1.   
];	
linesnb_e  = [
    4. 
	];
assert_checkequal(linesnb,linesnb_e);
assert_checkequal(couples,couples_e);

// Find the couples (c1,c2) and
// plots the lines for a=3,m=31
a = 3;
m = 31;
[linesnb,couples] = randnum_latticefind(a,m,2,20,%f);
couples_e  = [ 
  -3.    1.   
  -1.   -10.  
  -4.   -9.   
  -7.   -8.   
  -10.  -7.   
  -13.  -6.  
];	
linesnb_e  = [
    4. 
    11.
    13.
    15.
    17.
    19.
	];
assert_checkequal(linesnb,linesnb_e);
assert_checkequal(couples,couples_e);

//
a = 11;
m = 31;
[linesnb,couples] = randnum_latticefind(a,m,2,10);
couples_e  = [ -2.    3.  ];
linesnb_e  =   5.  ;
assert_checkequal(linesnb,linesnb_e);
assert_checkequal(couples,couples_e);

// Compute the minimum number of lines
// covering all points.
a = 12;
m = 31;
[linesnb,couples] = randnum_latticefind(a,m,2,20,%f);
couples_e  = [
  -2.   -5.   
  -5.    3.   
  -7.   -2.   
  -3.    8.   
  -12.   1.   
  -8.    11.  
  -19.  -1.   
	];
linesnb_e  =   [
    7. 
    8. 
    9. 
    11.
    13.
    19.
    20.];
assert_checkequal(linesnb,linesnb_e);
assert_checkequal(couples,couples_e);

// Try all primitive roots modulo 31
m = 31;
upperlanes = randnum_planenumber(m,2);
// pr = number_primitiveroot(m,%inf);
pr = [3 11 12 13 17 21 22 24];
for a = pr
    linesnb = randnum_latticefind(a,m,2,40);
    // ratio<=1, ratio==1 is good, ratio small is bad.
    ratio = linesnb/upperlanes;
    mprintf("a=%d, number of lines=%d, ratio=%.2f\n",..
	a,linesnb,ratio);
end

// Try all primitive roots modulo 47
m = 47;
// pr = number_primitiveroot(m,%inf);
pr = [5 10 11 13 15 19 20 22 23 26 ..
29 30 31 33 35 38 39 40 41 43 44 45];
upperlanes = randnum_planenumber(m,2);
mprintf("Upper number of planes:%d\n",upperlanes);
for a = pr
    linesnb = randnum_latticefind(a,m,2,40);
    ratio = linesnb/upperlanes;
    mprintf("a=%d, number of lines=%d, ratio=%.2f\n",..
	a,linesnb,ratio);
end
//
// Try the multiplier a=11 in dimension 3
a = 11;
m = 31;
[planesnb,uplets] = randnum_latticefind(a,m,3,10);
assert_checkequal(planesnb,4);
r = modulo(uplets(1)+uplets(2)*a+uplets(3)*a^2,m);
assert_checkequal(r,0);
//
// With an output function.
function myoutfun(step,k,c)
	if (step=="newk") then
		mprintf("%s: k=%d\n",..
		step,k)
	else
		mprintf("%s: k=%d, c=%s\n",..
		step,k,strcat(string(c)," "))
	end
endfunction
m=31;
a = 12;
[planesnb,uplets] = randnum_latticefind(a,m,2,%inf,[],myoutfun)
