// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// A LCG with composite modulus (full period)
m = 2^5;
a = 5;
b = 1;
tf = randnum_lcgfullperiod(a,b,m);
assert_checktrue(tf);

// A multiplicative generator with prime
// modulus (full period)
m = 31;
a = 3;
b = 0;
tf = randnum_lcgfullperiod(a,b,m);
assert_checktrue(tf);

// A multiplicative generator with composite
// modulus (full period)
m = 3*9; // 27
a = 2;
b = 0;
seed = 1;
tf = randnum_lcgfullperiod(a,b,m,seed);
assert_checktrue(tf);

// A multiplicative generator with composite modulus
// Not full period: 3 is not a primitive root modulo 27
m = 3*9; // 27
a = 3;
b = 0;
seed = 1;
tf = randnum_lcgfullperiod(a,b,m,seed);
assert_checkfalse(tf);

// A multiplicative generator with composite modulus
// Not full period: seed=3 is not relatively prime to 27
m = 3*9; // 27
a = 2;
b = 0;
seed = 3;
tf = randnum_lcgfullperiod(a,b,m,seed);
assert_checkfalse(tf);
