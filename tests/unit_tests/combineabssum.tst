// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// Find all couples (a(i),b(j)) such that |a(i)|+|b(j)| = 2
a = -3:3;
b = -2:2;
[values,indices] = randnum_combineabssum(2,a,b);
expected = [
  -2.   0.  
  -1.  -1.  
  -1.   1.  
   0.  -2.  
   0.   2.  
   1.  -1.  
   1.   1.  
   2.   0.  
];
assert_checkequal(values,expected);
expected = [
    2.    3.  
    3.    2.  
    3.    4.  
    4.    1.  
    4.    5.  
    5.    2.  
    5.    4.  
    6.    3.  
];
assert_checkequal(indices,expected);
S = sum(abs(values),"c");
assert_checkequal(S,2*ones(8,1));
// Find all couples (a(i),b(j)) such that |a(i)|+|b(j)| = 3
a = -3:3;
b = -2:2;
[values,indices] = randnum_combineabssum(3,a,b);
S = sum(abs(values),"c");
assert_checkequal(S,3*ones(10,1));
//
function checkcombineabssum(s,a,b)
	a = a(:)
	b = b(:)
	[values,indices] = randnum_combineabssum(s,a,b)
	m = size(values,"r")
	assert_checkequal(size(values),size(indices))
	assert_checkequal(a(indices(:,1)),values(:,1))
	assert_checkequal(b(indices(:,2)),values(:,2))
	assert_checkequal(sum(abs(values),"c"),s*ones(m,1));
endfunction
for s = 0:3:10
	mprintf("Checking s=%d\n",s)
	checkcombineabssum(s,-3:3,-2:2);
end
//
// Check with 4 arguments.
s = 0;
a = -3:3;
b = -2:2;
c = -1:1;
d = -2:2;
[values,indices] = randnum_combineabssum(s,a,b,c,d);
m = size(values,"r");
assert_checkequal(a(indices(:,1)),values(:,1)');
assert_checkequal(b(indices(:,2)),values(:,2)');
assert_checkequal(c(indices(:,3)),values(:,3)');
assert_checkequal(d(indices(:,4)),values(:,4)');
assert_checkequal(sum(abs(values),"c"),zeros(m,1));
