// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Plots the lines for a=3, m=31
// and the couple (c1=3,c2=-1)
scf();
a = 3;
b = 0;
m = 31;
randnum_lcgplot(a,b,m);
c1 = 3;
c2 = -1;
randnum_plotlines(c1,c2);
