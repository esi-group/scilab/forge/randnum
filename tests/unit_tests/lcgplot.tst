// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

scf();
randnum_lcgplot(3,0,31);

// Enable the tags
scf();
randnum_lcgplot(3,0,31,%t);

// Plot non-overlapping pairs
scf();
randnum_lcgplot(3,0,31,[],%f);

// Overlapping pairs from x=12*x (mod 31)
scf();
randnum_lcgplot(12,0,31);

// Figure 1.3: "Pairs of successive numbers from x=3*x (mod 31)
// Start from seed = 9
// Plot overlapping pairs
scf();
randnum_lcgplot(3,0,31,%t,[],9):
