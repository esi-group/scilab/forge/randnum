// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Numbers from x=3*x (mod 31)
// 3 is a primitive root modulo 31.
a = 3;
b = 0;
m = 31;
seed = 1;
[value,seed] = randnum_lcg(a,b,m,seed);
assert_checkalmostequal(value,0.0967742,[],1.e-7);
assert_checkequal(seed,3);
[value,seed] = randnum_lcg(a,b,m,seed);
assert_checkalmostequal(value,0.2903226,[],1.e-7);
assert_checkequal(seed,9);

// Compute all the values from this LCG.
a = 3;
b = 0;
m = 31;
seed = 9;
[value,seed] = randnum_lcg(a,b,m,seed,m-1);
assert_checkequal(size(value),[30 1]);
expected = [27 19 26 16 17 20 29 25 13 8 24 .. 
10 30 28 22 4 12 5 15 14 11 2 6 18 23 7 21 1 3 9];
expected = expected./m;
assert_checkequal(value,expected');

// Compute all the values from this LCG.
a = 3;
b = 0;
m = 31;
seed = 9;
[value,seed] = randnum_lcg(a,b,m,seed,15,2);
expected = [27 19 26 16 17 20 29 25 13 8 24 .. 
10 30 28 22 4 12 5 15 14 11 2 6 18 23 7 21 1 3 9];
expected = matrix(expected,2,15)'/31;
assert_checkequal(value,expected);
assert_checkequal(size(value),[15 2]);
