// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Reference
// Random number generation and Monte-Carlo methods
// James Gently, Second Edition
// Springer, 2005
// Section 1.2.1, Structure in the generated numbers
// 
y = [
    27.    29.    30.    15.    23.  
    19.    25.    28.    14.    7.   
    26.    13.    22.    11.    21.  
    16.    8.     4.     2.     1.   
    17.    24.    12.    6.     3.   
    20.    10.    5.     18.    9.   
];
y = y/31;
y = y(:);
r = randnum_autocorr(y,[0 1 2 3 4 5]);
e = [1.   0.27   0.16  -0.10   0.06   0.07]';
assert_checkalmostequal(r,e,[],1.e-2,"element");

// Reference:
// http://www.itl.nist.gov/div898/handbook/eda/section4/eda4251.htm
// "This data set was collected by H. S. Lew of NIST in 1969 to 
// measure steel-concrete beam deflections. 
// The response variable is the deflection of 
// a beam from the center point."
data = [
  -213.  -52.    201.   202.   194.   161.   172.   137.  -45.    68.   
  -564.   99.   -236.  -287.  -355.  -460.  -414.  -501.  -578.  -535.  
  -35.   -543.  -531.  -477.  -465.  -344.  -408.  -266.  -80.   -244.  
  -15.   -175.   83.    169.   156.   205.   188.   190.   138.   194.  
   141.   162.   27.   -124.  -81.   -281.  -125.  -391.  -462.  -351.  
   115.  -457.  -564.  -568.  -578.  -504.  -572.  -406.  -361.  -463.  
  -420.  -346.  -112.   17.   -64.    134.  -32.    194.   201.   174.  
  -360.   204.   131.   48.    139.  -28.    139.  -186.  -211.  -125.  
   203.  -300.  -507.  -568.  -449.  -576.  -492.  -553.  -554.  -570.  
  -338.  -474.  -254.  -135.  -384.  -118.  -321.   83.    32.    15.   
  -431.   164.   199.   162.   193.   156.   205.  -13.    74.    72.   
   194.  -107.  -311.  -430.  -198.  -437.  -262.  -577.  -533.  -550.  
  -220.  -572.  -495.  -422.  -538.  -381.  -504.  -49.   -235.  -190.  
  -513.  -8.     143.   172.   110.   200.   142.   103.   187.   172.  
   154.   83.   -46.   -74.   -44.   -220.  -83.   -515.  -372.  -424.  
  -125.  -541.  -579.  -577.  -577.  -540.  -574.  -280.  -442.  -385.  
  -559.  -224.  -90.   -13.   -6.     83.    0.     201.   182.   198.  
   92.    180.   136.   92.    66.    11.    48.    300.  -147.  -218.  
  -21.   -420.  -472.  -534.  -552.  -568.  -571.  -506.  -566.  -536.  
  -579.  -374.  -338.  -243.  -164.  -160.  -106.   131.   25.    96.   
	]
data = data(:);

// http://www.itl.nist.gov/div898/handbook/eda/section3/eda35c.htm
// [lag autocorrelation]
autocorrdata = [
  0.      1.00
  1.     -0.31
  2.     -0.74
  3.      0.77
  4.      0.21
  5.     -0.90
  6.      0.38
  7.      0.63
  8.     -0.77
  9.     -0.12
 10.      0.82
 11.     -0.40
 12.     -0.55
 13.      0.73
 14.      0.07
 15.     -0.76
 16.      0.40
 17.      0.48
 18.     -0.70
 19.     -0.03
 20.      0.70
 21.     -0.41
 22.     -0.43
 23.      0.67
 24.      0.00
 25.     -0.66
 26.      0.42
 27.      0.39
 28.     -0.65
 29.      0.03
 30.      0.63
 31.     -0.42
 32.     -0.36
 33.      0.64
 34.     -0.05
 35.     -0.60
 36.      0.43
 37.      0.32
 38.     -0.64
 39.      0.08
 40.      0.58
 41.     -0.45
 42.     -0.28
 43.      0.62
 44.     -0.10
 45.     -0.55
 46.      0.45
 47.      0.25
 48.     -0.61
 49.      0.14
];
for i = 1 : 50
	k = autocorrdata(i,1);
	e = autocorrdata(i,2);
	r = randnum_autocorr(data,k);
	assert_checkalmostequal(r,e,[],1.e-2);
end

