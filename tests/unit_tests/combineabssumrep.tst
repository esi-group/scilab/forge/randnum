// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// Find all (a(i)) such that |a(i)| = 2
values = randnum_combineabssumrep(2,1,-3:3);
expected = [
  -2.
   2.
];
assert_checkequal(values,expected);
// Find all couples (a(i),b(j)) such that |a(i)|+|b(j)| = 2
values = randnum_combineabssumrep(2,2,-3:3);
expected = [
  -2.   0.  
  -1.  -1.  
  -1.   1.  
   0.  -2.  
   0.   2.  
   1.  -1.  
   1.   1.  
   2.   0.  
];
assert_checkequal(values,expected);
//
// Get the indices
[values,indices] = randnum_combineabssumrep(2,2,-3:3);
expected = [
  -2.   0.  
  -1.  -1.  
  -1.   1.  
   0.  -2.  
   0.   2.  
   1.  -1.  
   1.   1.  
   2.   0.  
];
assert_checkequal(values,expected);
expected = [
    2.    3.  
    3.    2.  
    3.    4.  
    4.    1.  
    4.    5.  
    5.    2.  
    5.    4.  
    6.    3.  
];
//
// Get 3-uplets
values = randnum_combineabssumrep(2,3,-3:3);
expected = [
  -2.   0.   0.  
  -1.  -1.   0.  
  -1.   0.  -1.  
  -1.   0.   1.  
  -1.   1.   0.  
   0.  -2.   0.  
   0.  -1.  -1.  
   0.  -1.   1.  
   0.   0.  -2.  
   0.   0.   2.  
   0.   1.  -1.  
   0.   1.   1.  
   0.   2.   0.  
   1.  -1.   0.  
   1.   0.  -1.  
   1.   0.   1.  
   1.   1.   0.  
   2.   0.   0.  ];
assert_checkequal(values,expected);
