// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function values = randnum_combineabssumk(k,s,d)
    // Returns all d-uplets which absolute value sum to s.
    //
    // Calling Sequence
	// values = randnum_combineabssumk(k,s,d)
    // 
    // Parameters
    // k : a 1-by-1 matrix of doubles, integer value, the maximum value in the uplets
    // s : a 1-by-1 matrix of doubles, integer value, the sum
    // d : a 1-by-1 matrix of doubles, integer value, the dimension
    // values : a m-by-d matrix of doubles, integer value, the combinations
    //
    // Description
    // Returns all uplets (u1,u2,...,ud) 
    // where u1,u2,...,ud in {-k,-k+1,...,k} 
    // such that |u1|+|u2|+...+|ud|==s.
	//
	// The k-th uplet is 
    //<screen>
    // u1 = values(k,1)
    // u2 = values(k,2)
    // ...
    // ud = values(k,d)
    //</screen>
	//
	// for k=1,2,...,m.
	//
    // Examples
    // // All 2-uplets in {0,1,2,3,4} which abs sum to s=3.
    // values = randnum_combineabssumk(4,3,2)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_combineabssumk" , rhs , 3 )
    apifun_checklhs ( "randnum_combineabssumk" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "randnum_combineabssumk" , k , "k" , 1 , "constant" )
    apifun_checktype ( "randnum_combineabssumk" , s , "s" , 2 , "constant" )
    apifun_checktype ( "randnum_combineabssumk" , d , "d" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_combineabssumk" , k , "k" , 1 )
    apifun_checkscalar ( "randnum_combineabssumk" , s , "s" , 2 )
    apifun_checkscalar ( "randnum_combineabssumk" , d , "d" , 3 )
    //
    // Check content
    apifun_checkflint ( "randnum_combineabssumk" , k , "k" , 1 )
    apifun_checkgreq  ( "randnum_combineabssumk" , k , "k" , 1 , 0 )
    apifun_checkflint ( "randnum_combineabssumk" , s , "s" , 2 )
    apifun_checkgreq  ( "randnum_combineabssumk" , s , "s" , 2 , 0 )
    apifun_checkflint ( "randnum_combineabssumk" , d , "d" , 3 )
    apifun_checkgreq  ( "randnum_combineabssumk" , d , "d" , 3 , 1 )
	//
	if ( d==2 ) then
        V1 = randnum_combinesumk(k,s,d)
	    V2 = [V1(:,1),-V1(:,2)]
	    V3 = [-V1(:,1),V1(:,2)]
	    V4 = [-V1(:,1),-V1(:,2)]
        values = [V4;V3;V2;V1]
	else
	    values = []
        n = 0
        for i = 0:k
            V = randnum_combineabssumk(k,s-i,d-1)
            m = size(V,"r")
			//
            vi = ones(m,1)*i
            values(n+1:n+m,1:d) = [vi,V]
            n = n+m
			//
            vi = -ones(m,1)*i
            values(n+1:n+m,1:d) = [vi,V]
            n = n+m
			if (s==i) then
			break
			end
        end
	end
	values = gsort(values,"lr","i")
	values = unique(values,"r")
endfunction
