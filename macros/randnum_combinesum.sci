// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [values,indices] = randnum_combinesum(s,varargin)
    // Returns the combinations which sums to s.
    // 
    // Calling Sequence
    // values = randnum_combinesum(s,a1,a2)
    // values = randnum_combinesum(s,a1,a2,a3)
    // values = randnum_combinesum(s,a1,a2,a3,a4,...)
    // [values,indices] = randnum_combinesum(s,a1,...)
    // 
    // Parameters
    // s : a 1-by-1 matrix of doubles, integer value, the sum
    // a1 : a n1-by-1 matrix of doubles, integer value
    // a2 : a n2-by-1 matrix of doubles, integer value
    // a3 : a n3-by-1 matrix of doubles, integer value
    // a4 : a n4-by-1 matrix of doubles, integer value
    // values : a m-by-n matrix of doubles, where n is the number of arguments to combine and m is the number of matching combinations. On output, we have sum(values,"c")==s.
    // indices : a m-by-n matrix of doubles, where n is the number of arguments to combine  and m is the number of matching combinations. The indices corresponding to the arguments.
    //
    // Description
    // Returns all uplets (i1,i2,i3,...) such that 
    //<screen>
    // a1(i1)+a2(i2)+a3(i3)+...=s.
    //</screen>
	//
    // The k-th uplet is :
    //<screen>
    // i1 = indices(k,1)
    // i2 = indices(k,2)
    // i3 = indices(k,3)
    // ...
    //</screen>
	// and the corresponding values are 
    //<screen>
    // a1(i1) = values(k,1)
    // a1(i2) = values(k,2)
    // a1(i3) = values(k,3)
    // ...
    //</screen>
	// for k= 1, 2, ..., m.
	//
	// Up to 1000 arguments can be combined.
	//
    // Examples
	// // Search combinations of a1=-3:3 and a2=-2:2,
	// // which sum to zero.
    // values = randnum_combinesum(0,-3:3,-2:2)
    // sum(values,"r")
	//
	// // Get the indices
	// a1 = -3:3
	// a2 = -2:2
    // [values,indices] = randnum_combinesum(0,-3:3,-2:2)
	// // The line #1 is [2 5], meaning that 
	// a1(2) + a2(5) // is zero
	// // The first column of values:
	// values(:,1)
	// // ... corresponds to the first column of indices in a1 :
	// a1(indices(:,1))'
    //
	// // Search combinations which sum to 1
    // [values,indices] = randnum_combinesum(1,-3:3,-2:2)
	//
	// // Combine up to 4 arguments.
	// s = 0;
	// a1 = -3:3;
	// a2 = -2:2;
	// a3 = -1:1;
	// a4 = -2:2;
	// [values,indices] = randnum_combinesum(s,a1,a2)
	// [values,indices] = randnum_combinesum(s,a1,a2,a3)
	// [values,indices] = randnum_combinesum(s,a1,a2,a3,a4)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_combinesum" , rhs , 1:1000 )
    apifun_checklhs ( "randnum_combinesum" , lhs , 1:2 )
    //
    // Check type
    apifun_checktype ( "randnum_combinesum" , s , "s" , 1 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_combinesum" , s , "s" , 1 )
    for i = 1 : length(varargin)
        ai = varargin(i)
        apifun_checkvector ( "randnum_combinesum" , ai , "a"+string(i) , i )
    end
    //
    // Check content
    apifun_checkflint ( "randnum_combinesum" , s , "s" , 1 )
    for i = 1 : length(varargin)
        ai = varargin(i)
        apifun_checkflint ( "randnum_combinesum" , ai , "a"+string(i) , i )
    end
	//
	[values,indices] = rn_combinesum(s,varargin(:))
endfunction

function [U,I] = rn_combinesum(varargin)
    [lhs, rhs] = argn()
    s = varargin(1)
    if (rhs==2) then
		a = varargin(2)
		a = a(:)
		I = find(s==a)
		I = I(:)
		U = a(I)
        return
    end
    if (rhs==3) then
        [U,I] = rn_combinesum2(s,varargin(2),varargin(3))
        return
    end
    U = []
    I = []
    a = varargin(2)
    amin = min(a)
    amax = max(a)
    bmin = %inf
    bmax = -%inf
    for k = 3 : rhs
        bmin = min(min(varargin(k)),bmin)
        bmax = max(max(varargin(k)),bmax)
    end
    d = rhs-1
    if (amax+(d-1)*bmax<s) then
        return
    end
    if (amin+(d-1)*bmin>s) then
        return
    end
    n = 0
    a = a(:)
    imin = min(find(a>=s-(d-1)*bmax))
    imax = max(find(a<=s-(d-1)*bmin))
    for i = imin:imax
        [Us,Is] = rn_combinesum(s-a(i),varargin(3:$))
        m = size(Us,"r")
        if (m>0) then
            ii = i(ones(m,1))
            U(n+1:n+m,1:d) = [a(ii),Us]
            I(n+1:n+m,1:d) = [ii,Is]
            n = n+m
        end
    end
endfunction
function [U,I] = rn_combinesum2(s,a,b)
    U = []
    I = []
	a = a(:)
	b = b(:)
    amin = min(a)
    amax = max(a)
    bmin = min(b)
    bmax = max(b)
    if (amax+bmax<s) then
        return
    end
    if (amin+bmin>s) then
        return
    end
    n = 0
    imin = min(find(a>=s-bmax))
    imax = max(find(a<=s-bmin))
    for i = imin:imax
        j = find(a(i)==s-b)
        m = size(j,"*")
        if (m>0) then
            ii = i(ones(m,1))
            I(n+1:n+m,1:2)=[ii,j']
            U(n+1:n+m,1:2)=[a(ii),b(j)]
            n = n+m
        end
    end
endfunction
