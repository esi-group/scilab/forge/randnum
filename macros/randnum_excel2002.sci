// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [value,seed] = randnum_excel2002 ( varargin )
    // Random numbers as in Excel 2002.
    // 
    // Calling Sequence
    //   [value,seed]=randnum_excel2002(seed)
    //   [value,seed]=randnum_excel2002(seed,n)
    //   [value,seed]=randnum_excel2002(seed,n,d)
    //
    // Parameters
    // seed : a 1-by-1 matrix of doubles, a random number in the [0,1] interval
    // n : 1-by-1 matrix of doubles, integer value, the number of samples (default n=1)
    // d : 1-by-1 matrix of doubles, integer value, the dimension (default d=1)
    // value : n-by-d matrix of doubles, a pseudorandom value uniform in (0,1).
    //
    // Description
    // Returns random numbers as in Excel 2002.
    //
    //   "This formula will provide up to 1 million different numbers."
    //
    //   This random number generator is not used in Excel anymore.
    //
    //   "For Excel 2002 and earlier, both the ATP random number generator 
    //   and RAND were known to perform poorly on standard tests of randomness. 
    //   Performance was poor because the length of a cycle before the sequence 
    //   of pseudo-random numbers starts repeating was too short. 
    //   This is an issue only when many random numbers are required.
    //
    //   RAND has been improved for Excel 2003 and for later versions of Excel so 
    //   that RAND now passes all such standard tests."
    //
    // This random number is based on 
    //
    //<screen>
    //s = (a*s+b) mod m,
    //</screen>
    //
    // with
    //
    //<screen>
    // a = 9821
    // b = 211327
    // m = 1000000
    //</screen>
    //
    // Examples
    // seed = 0.5
    // [value,seed] = randnum_excel2002(seed)
    // [value,seed] = randnum_excel2002(seed)
    //
    // // Get the 10 first samples
    // seed = 0.5
    // [value,seed] = randnum_excel2002(seed,10)
    //
    // // Get 10 samples in dimension 2
    // seed = 0.5
    // [value,seed] = randnum_excel2002(seed,10,2)
	//
	// // Plot 500 samples in dimension 2
	// seed = 0.5;
	// [value,seed] = randnum_excel2002(seed,500,2);
	// plot(value(:,1),value(:,2),".");
	// xtitle("Excel 2002 Random Numbers","X1","X2")
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    //   http://support.microsoft.com/kb/86523/en
    //   http://support.microsoft.com/kb/828795/en-us

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_excel2002" , rhs , 1:3 )
    apifun_checklhs ( "randnum_excel2002" , lhs , 1:2 )
    //
    seed = varargin(1)
    n = apifun_argindefault ( varargin , 2 , 1 )
    d = apifun_argindefault ( varargin , 3 , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_excel2002" , seed , "seed" , 1 , "constant" )
    apifun_checktype ( "randnum_excel2002" , n , "n" , 2 , "constant" )
    apifun_checktype ( "randnum_excel2002" , d , "d" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_excel2002" , seed , "seed" , 1 )
    apifun_checkscalar ( "randnum_excel2002" , n , "n" , 2 )
    apifun_checkscalar ( "randnum_excel2002" , d , "d" , 3 )
    //
    // Check content
    apifun_checkrange ( "randnum_excel2002" , seed , "seed" , 1 , 0. , 1. )
    apifun_checkflint ( "randnum_excel2002" , n , "n" , 2 )
    apifun_checkgreq ( "randnum_excel2002" , n , "n" , 2 , 1 )
    apifun_checkflint ( "randnum_excel2002" , d , "d" , 3 )
    apifun_checkgreq ( "randnum_excel2002" , d , "d" , 3 , 1 )

    value = zeros(n,d)
    for i = 1 : n
        for j = 1 : d
            seed = 9821 * seed + 0.211327
            // The fractional part of seed
            seed = seed - int(seed) 
            value(i,j) = seed
        end
    end
endfunction

