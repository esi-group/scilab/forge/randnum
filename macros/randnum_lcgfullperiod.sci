// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function tf = randnum_lcgfullperiod(varargin)
    // Check if a LCG has full period.
    // 
    // Calling Sequence
    //  tf = randnum_lcgfullperiod(a,b,m)
    //  tf = randnum_lcgfullperiod(a,b,m,seed)
    //  
    // Parameters
    // a : a 1-by-1 matrix of doubles, integer value, positive, the multiplier of the LCG
    // b : a 1-by-1 matrix of doubles, integer value, positive, the shift of the LCG
    // m : a 1-by-1 matrix of doubles, integer value, positive, the modulo of the LCG
    // seed : a 1-by-1 matrix of doubles, integer value, a seed for the random number generator. The seed must be in the set 0,1,2,...,m-1. If b==0 and m is not prime, the seed is a required argument.
    // tf : a 1-by-1 matrix of booleans, true if the LCG has full period
    //
    // Description
    // Returns true if the linear congruential generator 
    // with modulus m, multiplier a and increment b has maximal 
    // period.
    //
    // If b is nonzero, the conditions are:
    //
    // (i) b is relatively prime to m
    //
    // (ii) a-1 is a multiple of p, for every prime p dividing m
    //
    // (iii) a-1 is a multiple of 4, if m is a multiple of 4.
    //
    // If b==0, the conditions are :
    //
    // (i) seed is relatively prime to m.
    //
    // (ii) a is a primitive root modulo m.
	//
	// If b==0 and m is prime, then the condition (i) is 
	// satisfied, for any seed. 
	// This is why, if b is zero and m is prime, the seed 
	// is not required in the calling sequence. 
    //
	// Suppose that m=2^e, with e>=4, b is odd
	// and a==5 (mod 8). 
	// Then the LCG has full period.
    //
    // The previous conditions are from Theorem A 
    // and theorem B in Knuth.
    //
    // Examples
    // // A LCG with composite modulus (full period)
    // m = 2^5;
    // a = 5;
    // b = 1;
    // tf = randnum_lcgfullperiod(a,b,m)
	//
	// // A multiplicative generator with prime 
	// // modulus (full period)
    // m = 31;
    // a = 3;
    // b = 0;
	// tf = randnum_lcgfullperiod(a,b,m)
	//
	// // A multiplicative generator with composite 
	// // modulus (full period)
    // m = 3*9; // 27
    // a = 2;
    // b = 0;
	// seed = 1;
	// tf = randnum_lcgfullperiod(a,b,m,seed)
	//
    // // A multiplicative generator with composite modulus
    // // Not full period: 3 is not a primitive root modulo 27
    // m = 3*9; // 27
    // a = 3;
    // b = 0;
	// seed = 1;
	// tf = randnum_lcgfullperiod(a,b,m,seed)
	//
    // // A multiplicative generator with composite modulus
    // // Not full period: seed=3 is not relatively prime to 27
    // m = 3*9; // 27
    // a = 2;
    // b = 0;
	// seed = 3;
	// tf = randnum_lcgfullperiod(a,b,m,seed)
	//
	// // Check that if m=2^e and e>=4 and 
	// // b is odd and a==5 (mod 8), then the sequence has full period.
	// m = 2^5
	// b = 1
	// a = 5
	// tf = randnum_lcgfullperiod(a,b,m)
	// // Check this empirically
	// seed = 0;
	// [U,seed] = randnum_lcg(a,b,m,seed,m);
	// V = gsort(U*m,"g","i")'
	// and(V==(0:m-1))
	//
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_lcgfullperiod" , rhs , 3:4)
    apifun_checklhs ( "randnum_lcgfullperiod" , lhs , 0:1 )
    //
    a = varargin(1)
    b = varargin(2)
    m = varargin(3)
    seed = apifun_argindefault ( varargin , 4 , [] )
    //
    if (b==0) then
		ispr = number_isprime(m)
		if (~ispr) then
			apifun_checkrhs ( "randnum_lcgfullperiod" , rhs , 4)
		end
    end
    //
    // Check type
    apifun_checktype ( "randnum_lcg" , a , "a" , 1 , "constant" )
    apifun_checktype ( "randnum_lcg" , b , "b" , 2 , "constant" )
    apifun_checktype ( "randnum_lcg" , m , "m" , 3 , "constant" )
    apifun_checktype ( "randnum_lcg" , seed , "seed" , 4 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_lcg" , a , "a" , 1 )
    apifun_checkscalar ( "randnum_lcg" , b , "b" , 2 )
    apifun_checkscalar ( "randnum_lcg" , m , "m" , 3 )
    if (seed<>[]) then
    apifun_checkscalar ( "randnum_lcg" , seed , "seed" , 4 )
	end
    //
    // Check content
    apifun_checkflint ( "randnum_lcg" , a , "a" , 1 )
    apifun_checkgreq ( "randnum_lcg" , a , "a" , 1 , 1 )
    apifun_checkflint ( "randnum_lcg" , b , "b" , 2 )
    apifun_checkgreq ( "randnum_lcg" , b , "b" , 2 , 0 )
    apifun_checkflint ( "randnum_lcg" , m , "m" , 3 )
    apifun_checkgreq ( "randnum_lcg" , m , "m" , 3 , 1 )
    if (seed<>[]) then
    apifun_checkflint ( "randnum_lcg" , seed , "seed" , 4 )
    apifun_checkrange ( "randnum_lcg" , seed , "seed" , 4 , 0 , m-1 )
	end
    //
    // Proceed...

    if (b==0) then
		if (ispr) then
			cond1 = %t
		else
			cond1 = number_coprime(seed,m)
		end
        cond2 = number_isprimroot(a,m)
        tf = (cond1 & cond2)
    else
        // (i) b is relatively prime to m
        cond1 = number_coprime(b,m)
        // (ii) a-1 is a multiple of p, for every prime p dividing m
        f = number_getprimefactors(m)
        cond2 = %t;
        for p = f'
            condp = number_isdivisor ( p , a-1 )
            cond2 = (cond2 & condp);
        end
        // (iii) a-1 is a multiple of 4, if m is a multiple of 4.
        if (number_isdivisor(4,m)) then
            cond3 = number_isdivisor(4,a-1)
        else
            cond3 = %t
        end
        tf = (cond1 & cond2 & cond3)
    end
endfunction
