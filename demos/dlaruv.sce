// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demoDlaruv()
    // In dimension 2
    s = [1 1 1 1];
    n = 2;
    rng = randnum_dlaruvstart ( n , s );
    X = [];
    for i = 1:100
        [ value , rng ] = randnum_dlaruv (rng);
        X($+1,:) = value';
    end
    h = scf();
    drawlater();
    plot(X(:,1),X(:,2),"bo")
    xtitle("Dlaruv: 100 points","X1","X2")
    h.children.children.children.mark_size = 1;
    drawnow();
endfunction 
demoDlaruv();
clear demoDlaruv

