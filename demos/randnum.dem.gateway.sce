// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

demopath = get_absolute_file_path("randnum.dem.gateway.sce");
subdemolist = [
"dlaruv", "dlaruv.sce"; ..
"excel2002", "excel2002.sce"; ..
"parkmiller", "parkmiller.sce"; ..
"shrage", "shrage.sce"; ..
"composite_modulus", "composite_modulus.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
