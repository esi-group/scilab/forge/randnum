// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Plot overlapping pairs
// http://www.cse.wustl.edu/~jain/cse567-11/ftp/k_27trg/sld032.htm
[planesnb,uplets] = randnum_latticefind(12,31,2,%inf)
ds = 1/sqrt(uplets(1,1)^2+uplets(1,2)^2)
d = (31/2)/sqrt(1+(5/2)^2)

// Reference
// "SPECTRAL TEST"
// http://random.mat.sbg.ac.at/tests/theory/spectral/
// d2 = 0.3162, 0.1162, 0.0790, 0.0632
// But we find : 
// d2 = 0.3162, 0.1162, 0.0791, 0.0526
// So the last distance is different. Is it wrong ?
m = 256;
for a = [85, 101, 61, 237]
	[planesnb,uplets] = randnum_latticefind(a,m,2,%inf);
	ds = 1/sqrt(uplets(1,1)^2+uplets(1,2)^2);
	mprintf("m=%d, a=%d, ds=%.4f\n",..
	m,a,ds)
end

//
// Search the best multiplier for modulus m=256 and b=1
m = 256;
b = 1;
dsmin = %inf;
for a = 1 : m-1
	tf = randnum_lcgfullperiod(a,b,m);
	if (tf) then
		[planesnb,uplets] = randnum_latticefind(a,m,2,%inf);
		ds = 1/sqrt(uplets(1,1)^2+uplets(1,2)^2);
		if (ds<dsmin) then
		dsmin = ds;
		mprintf("Full: m=%d, a=%d, b=%d, ds=%.4f\n",..
		m,a,b,ds)
		end
	end
end
