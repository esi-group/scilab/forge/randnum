// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function demoParkmiller()
    // In dimension 2
    X = [];
    seed = 1
    k = 1;
    for i = 1 : 100
        [ value , seed ] = randnum_parkmiller ( seed );
        X(k,1) = value;
        [ value , seed ] = randnum_parkmiller ( seed );
        X(k,2) = value;
        k = k + 1;
    end
    h = scf();
    drawlater();
    plot(X(:,1),X(:,2),"bo")
    xtitle("Parkmiller: 100 points","X1","X2")
    h.children.children.children.mark_size = 1;
    drawnow();
endfunction 
demoParkmiller();
clear demoParkmiller

